package zadanie;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "clinic")
public class Clinic {
	
	@XmlElementWrapper
	@XmlElement(name = "doctor")
	private List<Doctor> doctors;
	
	@XmlElementWrapper
	@XmlElement(name = "patient")
	private List<Patient> patients;

	@XmlElementWrapper
	@XmlElement(name = "appoitment")
	private List<Appoitment> appoitments;

	public Clinic(List<Doctor> doctors, List<Patient> patients, List<Appoitment> appoitments) {
		this.doctors = doctors;
		this.patients = patients;
		this.appoitments = appoitments;
	}

	public Clinic() {
	}

	public List<Doctor> getDoctors() {
		return doctors;
	}

	public void setDoctors(List<Doctor> doctors) {
		this.doctors = doctors;
	}

	public List<Patient> getPatients() {
		return patients;
	}

	public void setPatients(List<Patient> patients) {
		this.patients = patients;
	}

	public List<Appoitment> getAppoitments() {
		return appoitments;
	}

	public void setAppoitments(List<Appoitment> appoitments) {
		this.appoitments = appoitments;
	}
	
}
