package zadanie;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

//Lekarz(id, imie, nazwisko, specjalizacja), Pacjent(id, imie, nazwisko, wiek), Wizyta(id, data, koszt, przyczyna)
		//wizyta ma jednego lekarza i jednego pacjenta, lekarz ma wiele wizyt, pacjent tez ma wiele wizyt.
		
		//Company -> Przychodnia (lista lekarzy, lista, pacjentow, lista wizyt)
		//w mainie stworzyc jakas przychodnie z lekarzami, pacjentami, i wizytami
		//i sprobowac zmarshalowac obiekt przychodni.
		//chce aby w wizycie 
		//: <wizyta lekarz="id_lekarza" pacjent="id_pacjenta>
		//		<data></data>
		//		<koszt></koszt>
		//</wizyta>
public class MainMarshalling {
	public static void main(String[] args) throws JAXBException {
		Doctor d1 = new Doctor("Jan", "Kowaslki", "internista");
		Doctor d2 = new Doctor("Pawel", "Nowak", "dentysta");
		
		Patient p1 = new Patient("Karol", "Krawczyk", 23);
		Patient p2 = new Patient("Pawel", "Kowalczyk", 67);
		
		Appoitment a1 = new Appoitment(1, new Date(19-12-2017), 200, "leczenie kanalowe");
		Appoitment a2 = new Appoitment(2, new Date(21-12-2017), 20, "przeziebienie");
		
		//List<Doctor> doctors, List<Patient> patients, List<Appoitment> appoitments
		Clinic clinic = new Clinic(//
				Arrays.asList(d1.addAppoitment(a2), d2.addAppoitment(a1)),
				Arrays.asList(p1.addAppoitment(a2), p2.addAppoitment(a1)),
				Arrays.asList(a1, a2)
		);//

		JAXBContext jc = JAXBContext.newInstance(Clinic.class);
		Marshaller marshaller = jc.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

		marshaller.marshal(clinic, System.out);
	}
}
