package zadanie;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "patient")
public class Patient {
//Pacjent(id, imie, nazwisko, wiek)
	private Long id;
	private String name;
	private String surname;
	private int age;
	private static long GENERATOR = 1;
	
	@XmlElementWrapper
	@XmlElement(name = "appoitment")
	private List<Appoitment> appoitments;
	
	public Patient(String name, String surname, int age) {
		this.id = GENERATOR++;
		this.surname = surname;
		this.age = age;
		this.appoitments = new ArrayList<Appoitment>();
	}

	public Patient(){
		
	}
	
	public List<Appoitment> getAppoitments() {
		return appoitments;
	}
	
	public Patient addAppoitment(Appoitment app) {
		appoitments.add(app);
		app.setPatient(this);
		return this;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Patient [id=" + id + ", name=" + name + ", surname=" + surname + ", age=" + age +  "]";
	}

}
