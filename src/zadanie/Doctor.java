package zadanie;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "doctor")
public class Doctor {
	//(id, imie, nazwisko, specjalizacja)
	private Long id;
	private String name;
	private String surname;
	private String specalization;
	private static long GENERATOR = 1;
	
	@XmlElementWrapper
	@XmlElement(name = "appoitment")
	private List<Appoitment> appoitments;
	
	public Doctor(String name, String surname, String specalization) {
		this.id = GENERATOR++;
		this.surname = surname;
		this.specalization = specalization;
		this.appoitments = new ArrayList<Appoitment>();
	}
	
	public Doctor(){	
	}
	
	public Doctor addAppoitment(Appoitment app) {
		appoitments.add(app);
		app.setDoctor(this);
		return this;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getSpecalization() {
		return specalization;
	}
	public void setSpecalization(String specalization) {
		this.specalization = specalization;
	}
	public List<Appoitment> getAppoitments() {
		return appoitments;
	}
	public void setAppoitments(List<Appoitment> appoitments) {
		this.appoitments = appoitments;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "Doctor [id=" + id + ", name=" + name + ", surname=" + surname + ", specalization=" + specalization
				+ ", appoitments=" + appoitments + "]";
	}
		
}
