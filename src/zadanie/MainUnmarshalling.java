package zadanie;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.omg.Messaging.SyncScopeHelper;

public class MainUnmarshalling {

	public static void main(String[] args) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(Clinic.class);
		Unmarshaller unmarshaller = context.createUnmarshaller();

		Clinic clinic = (Clinic) unmarshaller.unmarshal(new File("clinic.xml"));

		clinic.getDoctors().forEach(System.out::println);
		clinic.getPatients().forEach(System.out::println);
		clinic.getAppoitments().forEach(System.out::println);
		
		for(Doctor d: clinic.getDoctors()){
			System.out.println(d+" "+d.getAppoitments());
			
		}
		
		for(Patient p : clinic.getPatients()){
			System.out.println(p+" "+p.getAppoitments());
			
		}
	}

}
