package zadanie;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "appoitment")
public class Appoitment {
//Wizyta(id, data, koszt, przyczyna)
	private int id;
	private Date date;
	private float price;
	private String reasone;
	
	@XmlAttribute(name = "doctor")
	@XmlJavaTypeAdapter(DoctorAdapter.class)
	private Doctor doctor;
	
	@XmlAttribute(name = "patient")
	@XmlJavaTypeAdapter(PatientAdapter.class)
	private Patient patient;
	
	public Appoitment(int id, Date date, float price, String reasone) {
		this.id = id;
		this.date = date;
		this.price = price;
		this.reasone = reasone;
	}
	
	public Appoitment(){
		
	}
	
	public Doctor getDoctor() {
		return doctor;
	}



	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}



	public Patient getPatient() {
		return patient;
	}



	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getReasone() {
		return reasone;
	}

	public void setReasone(String reasone) {
		this.reasone = reasone;
	}

	@Override
	public String toString() {
		return "Appoitment [id=" + id + ", date=" + date + ", price=" + price + ", reasone=" + reasone + "]";
	}
}
