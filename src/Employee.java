import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "employee")
public class Employee {
	private static long GENERATOR = 0;

	private Long id;

	private String name;
	private String surname;
	private String position;
	private int salary;

	//@XmlTransient
	@XmlElementWrapper
	@XmlElement(name = "laptop")
	private List<Laptop> laptops;

	public Employee(String name, String surname, String position, int salary) {
		this.id = GENERATOR++;
		this.name = name;
		this.surname = surname;
		this.position = position;
		this.salary = salary;
		this.laptops = new ArrayList<>();
	}

	public Employee() {
	}

	public List<Laptop> getLaptops() {
		return laptops;
	}

	public void setLaptops(List<Laptop> laptops) {
		this.laptops = laptops;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public Employee withLaptop(Laptop laptop) {
		laptops.add(laptop);
		laptop.setEmployee(this);
		return this;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", surname=" + surname + ", position=" + position + ", salary=" + salary
				+ "]";
	}

}
