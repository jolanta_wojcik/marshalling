import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class MainUnmarshalling {

	// dwie metody parsowania XML
	// DOM - Domain Object Model -> wczytuje caly xml do pamieci i wymaga
	// obiektow
	// SAX - ? -> operuje "w locie" na XMLu nie wczytujac go do pamieci ale
	// rejesturje eventy dla kazdego rodzaju node'a

	// jak masz plik XML zajmuje : 5GB -> !DOM -> tylko SAX
	public static void main(String[] args) throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(Company.class);
		Unmarshaller unmarshaller = context.createUnmarshaller();

		Company company = (Company) unmarshaller.unmarshal(new File("employeesWithLaptops.xml"));

		company.getEmployees().forEach(System.out::println);
		company.getLaptops().forEach(System.out::println);
		
		for(Laptop l: company.getLaptops()){
			System.out.println(l+" "+l.getEmployee());
			
		}
		// XSLT -> transformaty XML
	}
}
