import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "company")
public class Company {

	@XmlElementWrapper
	@XmlElement(name = "employee")
	private List<Employee> employees;

	@XmlElementWrapper
	@XmlElement(name = "laptop")
	private List<Laptop> laptops;

	public Company(List<Employee> employees, List<Laptop> laptops) {
		this.employees = employees;
		this.laptops = laptops;
	}

	public Company() {
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public List<Laptop> getLaptops() {
		return laptops;
	}

	public void setLaptops(List<Laptop> laptops) {
		this.laptops = laptops;
	}

}
