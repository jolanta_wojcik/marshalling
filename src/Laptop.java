import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "laptop")
public class Laptop {
	private String name;
	private int ram;
	private int hdd;

	@XmlAttribute(name = "owner")
	@XmlJavaTypeAdapter(EmployeeAdapter.class)
	private Employee employee;

	public Laptop(String name, int ram, int hdd) {
		this.name = name;
		this.ram = ram;
		this.hdd = hdd;
	}

	public Laptop() {
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRam() {
		return ram;
	}

	public void setRam(int ram) {
		this.ram = ram;
	}

	public int getHdd() {
		return hdd;
	}

	public void setHdd(int hdd) {
		this.hdd = hdd;
	}

	@Override
	public String toString() {
		return "Laptop [name=" + name + ", ram=" + ram + ", hdd=" + hdd + "]";
	}

}
