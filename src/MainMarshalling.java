import java.util.Arrays;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class MainMarshalling {
	public static void main(String[] args) throws JAXBException {
		// Marshalling -> zamiana obiektu na XML
		// Unmarshalling -> zamiana XML na obiekt

		// stworz 5ciu pracownikow
		Employee emp1 = new Employee("ania", "kowlska", "ksiegowy", 4000);
		Employee emp2 = new Employee("ania", "maj", "ksiegowy", 6000);
		Employee emp3 = new Employee("jan", "kowlski", "menager", 5000);
		Employee emp4 = new Employee("ala", "kwiecen", "nauczycie", 4000);
		Employee emp5 = new Employee("janusz", "kowlski", "prawnik", 7000);
		// stworz obiekt employees i zrob go z ta lista 5ciu pracownikow

		Laptop l1 = new Laptop("Sony", 16, 2);
		Laptop l2 = new Laptop("Asus", 8, 2);
		Laptop l3 = new Laptop("Acer", 16, 1);

		Company emps = new Company(//
				Arrays.asList(emp1, //
						emp2.withLaptop(l1), //
						emp3, //
						emp4.withLaptop(l2).withLaptop(l3), //
						emp5//
				), //
				Arrays.asList(l1, l2, l3)//
		);//

		JAXBContext jc = JAXBContext.newInstance(Company.class);
		Marshaller marshaller = jc.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

		marshaller.marshal(emps, System.out);

	}
}
