import javax.xml.bind.annotation.adapters.XmlAdapter;

public class EmployeeAdapter extends XmlAdapter<Long, Employee> {

	@Override
	public Long marshal(Employee arg0) throws Exception {
		return arg0.getId();
	}

	@Override
	public Employee unmarshal(Long id) throws Exception {
		//bazaDanych.znajdzPoId(id)
		return new Employee("TEST_"+id, null, null, 0);
	}

}
